function generate_table() {
    // get the reference for the body
    var body = document.getElementsByTagName("body")[0];
    var rowsCount = document.getElementsByClassName("input-rows")[0];
    var columnsCount = document.getElementsByClassName("input-columns")[0];
    const form = document.getElementById('form');
    form.addEventListener('submit', (e)=>{
      e.preventDefault()
    });
    // creates a <table> element and a <tbody> element
    var tbl = document.createElement("table");
    var prevtbl = document.getElementById("table");
    if(prevtbl!=null){
        prevtbl.remove();
    }
    var tblBody = document.createElement("tbody");
    // creating all cells
    for (var i = 0; i < rowsCount.value; i++) {
      // creates a table row
      var row = document.createElement("tr"); 
      for (var j = 0; j < columnsCount.value; j++) {
        // Create a <td> element and a text node, make the text
        // node the contents of the <td>, and put the <td> at
        // the end of the table row
        var cell = document.createElement("td");
        var cellText = document.createTextNode((i+1).toString()+(j+1).toString());
        cell.appendChild(cellText);
        cell.onclick = ChangeBackgroundColor;
        row.appendChild(cell);
      }  
      // add the row to the end of the table body
      tblBody.appendChild(row);
    }
    // put the <tbody> in the <table>
    tbl.setAttribute("id","table");
    tbl.appendChild(tblBody);
    // appends <table> into <body>
    body.appendChild(tbl);
  }

  function ChangeBackgroundColor(){
    var elem = this;
    const randomColor = Math.floor(Math.random()*16777215).toString(16);
    elem.style.backgroundColor = elem.style.backgroundColor ? '' : '#' + randomColor;
  }